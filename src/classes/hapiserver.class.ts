import Hapi from '@hapi/hapi';

/**
* Extends `Hapi.Server` class to accept costumizable `config` attribute.
* @extends Hapi.Server
**/
export class HapiServer extends Hapi.Server{
    public config: any;
}
