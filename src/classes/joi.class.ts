import joi from 'joi';

const ObjectIdPattern = /^[0-9a-fA-F]{24}$/;
const UserUsernamePattern = /^[0-9a-zA-Z]{4,15}$/;
const EmailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const UserPasswordPattern = /^\w(?=.*?[#?!@$%^&*-]).{6,20}$/;
const JWTPattern = /(^[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*$)/;
const RolePattern = /^(USER|ADMIN)$/;

/**
* Extends Joi class to accept `Joi.ObjectId()`, `Joi.UserUsername()`, 
* `Joi.UserEmail()`, `Joi.UserPassword()` and `Joi.JWT()` through
* testing regexs for each. 
* @extends Joi
* @param  joi.Extension[]
* @return Result of Joi validation `{value , errors}`
* @see `JoiSchema`
**/
const Joi = joi.extend(
    {
        type: 'ObjectId',
        messages: { invalid: 'El campo debe ser un ObjectId valido.'},
        validate(value, { error }) {
            if (!ObjectIdPattern.test(String(value))) 
                return { value, errors: error('invalid')}
        }
    },
    {
        type: 'Username',
        messages: { invalid: 'El campo debe ser un nombre de usuario valido.'},
        validate(value, {error}){
            if(!UserUsernamePattern.test(String(value)))
                return { value, errors: error('invalid')}
        }
    },
    {
        type: 'Email',
        messages: { invalid: 'El campo debe ser un correo electrónico valido.'},
        validate(value, {error}){
            if(!EmailPattern.test(String(value)))
                return { value, errors: error('invalid')}
        }
    },
    {
        type: 'UserPassword',
        messages: { invalid: 'El campo debe ser una contraseña valida.'},
        validate(value, {error}){
            if(!UserPasswordPattern.test(String(value)))
                return { value, errors: error('invalid')}
        }
    },
    {
        type: 'JWT',
        messages: { invalid: 'El campo debe ser un JWT válido.'},
        validate(value, {error}){
            if(!JWTPattern.test(String(value)))
                return { value, errors: error('invalid')}
        }
    },
    {
        type: 'Role',
        messages: {invalid: 'El campo debe ser un rol valido.'},
        validate(value, {error}){
            if(!RolePattern.test(String(value)))
                return { value, errors: error('invalid')}
        }
    }
)

export default Joi