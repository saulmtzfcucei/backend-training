import hapi from '@hapi/hapi';
import Boom from '@hapi/boom';
import bcrypt from 'bcrypt'

import { UserModel } from '../models/user.model';
import { generarJWT } from '../auth/generate-jwt';


/**
* Controller for user login 
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The response toolkit. 
* @return 
* * A 200 OK response with the obtained resource `{token}`
* * A 400 Bad Request error
* * A 500 Server Error response
* @see `JoiSchema`
**/
export const login = async(req : hapi.Request | any, h : any) => {
    try{
        const user = await UserModel.findOne({ email: req.payload.email }).where({deleted:false});
        if(!user) return Boom.badRequest('Los datos no son correctos');
        
        const validPassword = bcrypt.compareSync(req.payload.password, JSON.parse(JSON.stringify(user?.password)));
        if(!validPassword) return Boom.badRequest('Los datos no son correctos');

        const token = generarJWT(user?._id);
        if(!token) return Boom.badRequest('No se pudo generar el token');

        return h.response({ token }).code(200);
        
    }catch( error ){
        return Boom.badImplementation();
    }
}