import hapi from '@hapi/hapi';
import Boom from '@hapi/boom';
import bcrypt from 'bcrypt';

import { UserModel } from '../models/user.model';


/**
* Controller for getting one user from the database with a provided `id`. 
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit. 
* @return 
* * A 200 OK response with the obtained resource `{user}`
* * A 404 Not Found error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
export const getUser = async (req:hapi.Request, h:any) => {
    try{    
        const user = await UserModel.findById(req.params._id).where({deleted:false}).select('-password');
        return user ? 
            h.response(user?.toJSON()).code(200) : 
            Boom.notFound('No hay usuarios con ese id');
    }catch( error ){
        return Boom.badImplementation();
    }
}


/**
* Controller for posting a user in the database. 
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit. 
* @return 
* * A 200 OK response with the inserted resource `{user}`
* * A 400 Bad Request error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
export const postUser = async(req: hapi.Request | any, h:any) => {
    try{        
        const salt = bcrypt.genSaltSync();
        req.payload.password = bcrypt.hashSync(req.payload.password, salt);
        
        const user = await new UserModel(req.payload).save();
        user.password = undefined;
        
        return user ? 
            h.response(user?.toJSON()).code(200) : 
            Boom.badImplementation();
    }catch( error:any ){
        return error.code===11000 ? 
            Boom.badRequest(`El ${Object.keys(JSON.parse(JSON.stringify(error.keyValue)))[0]} ya ha sido utilizado`) :
            Boom.badImplementation();
    }
}


/**
* Controller for updating the username of a resource in the database. 
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit. 
* @return 
* * A 200 OK response with the updated resource `{user}`
* * A 400 Bad Request error response.
* * A 404 Not Found error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
export const putUser = async(req:hapi.Request | any , h:any) => {
    try{
        if(req.payload.password){
            const salt = bcrypt.genSaltSync();
            req.payload.password = bcrypt.hashSync(req.payload.password, salt);
        }

        const updatedUser = await UserModel.findByIdAndUpdate(req.params._id, req.payload, {new:true}).where({deleted:false}).select('-password');
        return updatedUser ? 
            h.response(updatedUser?.toJSON()).code(200) : 
            Boom.notFound('No hay usuarios con ese ID');
    }catch( error:any ){
        return error.code===11000 ? 
            Boom.badRequest(`El ${Object.keys(JSON.parse(JSON.stringify(error.keyValue)))[0]} ya ha sido utilizado`) :
            Boom.badImplementation();
    }
}


/**
* Controller for deleting one user from the database. 
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The response toolkit. 
* @return 
* * A 200 OK response with the updated resource `{user}`
* * A 404 Not Found error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
export const deleteUser = async(req:hapi.Request, h:any) => {
    try{
        const deletedUser = await UserModel.findByIdAndUpdate(req.params._id, {$set: {deleted:true}}, {new:true}).where({deleted:false}).select('-password');
        return deletedUser ? 
            h.response(deletedUser?.toJSON()).code(200) : 
            Boom.notFound(`No existe un usuario con dicho ID`);
    }catch( error ){
        return Boom.badImplementation();
    }
}