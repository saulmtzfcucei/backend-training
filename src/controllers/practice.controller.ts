import hapi from '@hapi/hapi';
import Boom from '@hapi/boom';
import { PracticeModel } from '../models/practice.model';

export const postPractice = async(req : hapi.Request, h:any) => {
    try {
        const practica = await new PracticeModel().save();

        return practica ? h.response(JSON.parse(JSON.stringify(practica))).code(200) : Boom.badImplementation();
    } catch (error) {
        console.log(error);
        return Boom.badImplementation();
    }
}