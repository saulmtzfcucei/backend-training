import * as auth from 'hapi-auth-jwt2';
import Boom from "@hapi/boom";
import hapi from '@hapi/hapi';

import { UserModel } from '../models/user.model';
import { HapiServer } from "../classes/hapiserver.class";

/**
* Validates a given `token` is a valid and active JWT. 
* @param artifacts - An object that contains information from the token
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit. 
* @return `isValid`. Boolean which response indicates wether token was valid or not.
**/
export const validate = async(decoded:any, req : hapi.Request | any, h:any) => {
    try{
        const user = await UserModel.findById(decoded._id).select('-password');
        return user ? { isValid: true } : { isValid: false };
    }catch(error){
        return Boom.badImplementation();
    }
}

/**
* Registers authentication strategies for Hapi server.
* Includes strategy for: JWT.
* @param  HapiServer
* @return context if the authentication failed. 
**/
export const auth_strategies = async(server : HapiServer) => {
    try{
        await server.register(auth);
        server.auth.strategy('jwt', 'jwt', {
            key: server.config.Auth.auth_secret,
            validate,
            verifyOptions: { algorithms: ['HS256'] },
            errorFunc: (context: any) => {
                return context;
            }
        });
        server.auth.default('jwt');
    }catch(error){
        return Boom.badImplementation();
    }
}