import Boom from '@hapi/boom';
import jwt from 'jsonwebtoken';

import { Yaml } from '../helpers/yaml.helper';

/**
* Function that generates a JWT. 
* @param  _id Receives a user id to generate a JWT.
* @return A generated `token`
**/
export const generarJWT = (_id:any) => {
    try{
        const { Auth } = Yaml();
    
        return jwt.sign( { _id } , Auth.auth_secret, {
            expiresIn: Auth.expiration_time
        });
    }catch( error ){
        return Boom.badImplementation();
    }
}