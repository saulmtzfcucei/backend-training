import { CronJob } from 'cron';
import { UserModel } from '../models/user.model';


export async function cronjobs(){
    try {
        const user_id : string = '621d0f83b4f2985eab55f22c';
        const cronJob : CronJob = new CronJob('* * * * * *', async() => {

            const date = new Date();
            const hours = date.getHours();
            const minutes = date.getMinutes();
            const seconds = date.getSeconds();

            const new_result = `h${hours}m${minutes}s${seconds}`;
            await UserModel.findByIdAndUpdate(user_id, {'username': new_result}); 
        });
        cronJob.start();
    } catch (error) {
        console.log(error);
    }
}