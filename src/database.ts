import mongoose from 'mongoose';
import { HapiServer } from './classes/hapiserver.class';

/**
* Async function that connects to the mongodb database 
* through mongoose library.
* @param server - Hapi server provided with config file that 
* contains the connection route.
* @exports `database` 
**/
export async function database(server : HapiServer){
    try{
        await mongoose.connect(server.config.Server.DB_Connection);
        console.log('Database is connected');

    }catch( error ){
        console.log(error);
    }
}