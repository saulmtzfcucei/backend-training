import YAML from 'yaml';
import fs from 'fs';

/**
* Function that returns server variables defined in a YAML file.
* @return `JSON` object containing the server variables.  
**/
export function Yaml(){
    const file = fs.readFileSync('./server-variables.yml', 'utf-8');
    return YAML.parse(file);
}