import { RouteOptions } from "@hapi/hapi"
import Joi from "../classes/joi.class"
import { JoiSchema, failAction } from "../models/joi.model"


/**
* Options object to receive a user resource given an id.
* with a `JoiSchema.user.methods.get` response schema. 
* @see `JoiSchema`
* @see `user_routes`
* @exports Options_GET_User 
**/
export const Options_GET_User : RouteOptions = {
    auth: 'jwt',
    description: 'Returns a resource within usuario collection given an id',
    validate: {
        params: Joi.object({ _id: JoiSchema.user.params._id }),
        failAction
    },
    response: {
        schema: JoiSchema.user.methods.get,
        failAction
    }
}


/**
* Options object to POST a user resource given a user payload.
* with a `JoiSchema.user.methods.get` response schema. 
* @see `JoiSchema`
* @see `user_routes`
* @exports Options_POST_User 
**/
export const Options_POST_User : RouteOptions = {
    auth: false,
    description: 'Inserts a resource within usuario collection',
    validate: {
        payload: JoiSchema.user.methods.post,
        failAction
    },
    response: {
        schema: JoiSchema.user.methods.get,
        failAction
    }
}


/**
* Options object to update a user given an id for accepted fields.
* with a `JoiSchema.user.methods.get` response schema. 
* @see `JoiSchema`
* @see `user_routes`
* @exports Options_PUT_User 
**/
export const Options_PUT_User : RouteOptions = {
    auth : 'jwt',
    description: 'Updates a resource within usuario collection given an id and username',
    validate: {
        params: Joi.object({_id: JoiSchema.user.params._id}),
        payload: JoiSchema.user.methods.put,
        failAction
    },
    response: {
        schema: JoiSchema.user.methods.get,
        failAction
    }
}

/**
* Options object to put user deleted field as true. 
* @see `JoiSchema`
* @see `user_routes`
* @exports Options_DELETE_User 
**/
export const Options_DELETE_User : RouteOptions = {
    auth : 'jwt',
    description: 'Deletes a resource within usuario collection given an id',
    validate: {
        params: Joi.object({ _id: JoiSchema.user.params._id }),
        failAction 
    },
    response: {
        schema: JoiSchema.user.methods.get,
        failAction
    }
}