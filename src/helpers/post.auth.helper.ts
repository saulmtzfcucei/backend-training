import { RouteOptions } from '@hapi/hapi';
import { failAction, JoiSchema } from '../models/joi.model';

/**
* Options object for user login authentication at *POST* `/login`. 
* @see `JoiSchema`
* @see `auth_routes`
* @exports Options_POST_Login 
**/
export const Options_POST_Login : RouteOptions = {
    auth: false,
    validate : {
        payload : JoiSchema.user.methods.login,
        failAction
    },
    response: {
        schema: JoiSchema.user.params.token,
        failAction
    }
}