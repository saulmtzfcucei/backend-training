import { RouteOptions } from "@hapi/hapi";
import { JoiSchema, failAction } from "../models/joi.model";


export const postPractice_Options : RouteOptions = {
    auth: 'jwt',
    description: 'Creates a resourse within practice collection',
    validate : {
        payload: JoiSchema.practice.methods.post,
        failAction
    },
    response : {
        schema : JoiSchema.practice.methods.get,
        failAction
    }
}