import { Schema, model } from 'mongoose';
/**
* User interface for 'usuario' collection, includes the types: 
* * username : `String`
* * email : `String`
* * password? : `String`
* * role : `String`
* * deleted : `Boolean`
* @see `userSchema`
**/
interface IUser{
    username: String; 
    email: String;
    password?: String;
    role : String;
    deleted : Boolean;
}

/**
* User schema for 'usuario' collection, of type 
* `<IUser>`. It does not allow versionKeys.
* Includes the types: 
* * username : `{type:String, required: true}`
* * email : `{type:String, required: true}`
* * password? : `{type:String}`
* * role `{ type:String, required:true}`
* * deleted `{type: Boolean, default:false, required:true}`
* @see `IUser`
**/
const userSchema = new Schema<IUser>({
    username: {
        type: String, 
        unique: true, 
        required: true
    },
    email: {
        type: String, 
        unique: true, 
        lowercase:true, 
        required: true
    },
    password: {
        type: String,
    },
    role : {
        type: String, 
        required: true, 
        default:'USER'
    },
    deleted: {
        type: Boolean, 
        default:false, 
        required: true
    }
},{
    versionKey: false,
    collection: 'usuario'
});

export const UserModel = model<IUser>('UserModel', userSchema);