import hapi from '@hapi/hapi';
import Boom from '@hapi/boom';

import Joi from './../classes/joi.class';

/**
* Definition of all Joi option objects, including:
* * Validation of user params
* * Validation of token
* * Validation of methods: `get`, `post`, `login`, `put`
* @exports JoiSchema 
**/
export const JoiSchema = {
    user : {
        params : {
            _id : Joi.ObjectId().required(),
            username : Joi.Username().required(),
            email : Joi.Email().required(),
            password : Joi.UserPassword().required(),
            role : Joi.Role().required(),
            deleted : Joi.boolean().required(),
            token : Joi.object({token : Joi.JWT().required()})
        },

        methods: {
            get: Joi.object({
                _id: Joi.ObjectId().required(), 
                username: Joi.Username().required(),
                email: Joi.Email().required(),
                role : Joi.Role().required(),
                deleted : Joi.boolean().required(),
            }),
            post: Joi.object({
                username: Joi.Username().required(),
                email : Joi.Email().required(),
                password: Joi.UserPassword().required(),
                role : Joi.Role(),
            }),
            login: Joi.object({
                email : Joi.Email().required(),
                password : Joi.UserPassword().required()
            }),
            put: Joi.alternatives().try(
                Joi.object({
                    username : Joi.Username(),
                    email : Joi.Email(),
                    password: Joi.UserPassword(),
                    role : Joi.Role()
                })
            ).required()
        }
    },
    practice : {
        methods : {
            post : Joi.any(),
            get : Joi.any()
        }
    }
}

/**
* failAction function if a validation using `JoiSchemas` failed
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit. 
* @param error - To receive errors.
* @see `JoiSchema`
* @exports `failAction` 
* @return
* * A 401 Unauthorized  error response.
* * A 500 Bad Implementation error response.
**/
export const failAction = ((req : hapi.Request, h : any, error : any) => {
    return error.isJoi ? 
        Boom.unauthorized(error.details[0].message) :
        Boom.badImplementation()
})