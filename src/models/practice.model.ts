import { Schema, model, Types } from 'mongoose';

interface IPractice{
    
    objType1? : Object;
    objType2? : Object;
    objType3? : Object;

    stringType1? : String;
    stringType2? : String;
    stringType3? : String;

    objectIdType1? : String;
    objectIdType2? : String;

    numberType1? : Number;
    numberType2? : Number;

    boolType1? : Boolean;
    boolType2? : Boolean;

    arrType1? : Array<Date>;
    arrType2? : Array<Object>;

    arrType3? : Array<Array<Date>>;
    arrType4? : Array<Array<Object>>;
    arrType5? : Array<Array<Array<Object>>>;
    arrType6? : Array<Array<Array<Date>>>;
}

const practiceSchema = new Schema<IPractice>({
    objType1 : { 
        property1_nestingLevel1 : {
            type: String,
            required: true,
            default: 'anidacion1'
        },
        property2_nestingLevel1 : {
            type: String,
            required: true,
            default: 'anidacion1'
        },
        property3_nestingLevel2 : {
            property_1 : {
                type: String,
                required: true,
                default : 'sub2'
            },
            property_2 : {
                type : [{
                    _id: false,
                    date : {
                        type: Date,
                        required : true,
                        default: new Date()
                    }
                }]
            }
        },
        property4_nestingLevel3 : {
            property_1 : {
                property_1 : {
                    type : {
                        type: String,
                        required: true,
                        default : 'sub3'
                    }
                },
                property_2 : {
                    type : [{
                        _id: false,
                        date : {
                            type: Date,
                            required: true,
                            default: new Date()
                        }
                    }]
                }
            }
        }
    },
    objType2 : {
        property1_nestingLevel5 : {
            lvl2 : {
                lvl3 : {
                    lvl4 : {
                        lvl5 : {
                            property_1 : {
                                type : {
                                    type: String,
                                    required: true,
                                    default : 'sub3'
                                }
                            },
                            property_2 : {
                                type : [{
                                    _id: false,
                                    date : {
                                        type: Date,
                                        required: true,
                                        default: new Date()
                                    }
                                }]
                            }
                        }
                    }
                }
            }
        }
    },
    objType3 : {
        array : {
            type : [{
                _id : false,
                array : [{
                    _id : false,
                    array : [{
                        _id : false,
                        array : [{
                            _id : false,
                            date : {
                                type: Date,
                                required: true,
                                default: new Date()
                            }
                        }]
                    }]
                }]
            }]
        }
    },
    stringType1 : {
        type: String,
        required: true,
        default : 'string1'
    },
    stringType2 : {
        type: String,
        required: true,
        default : 'string2'
    },
    stringType3 : {
        type: String,
        required: true,
        default : 'string3'
    },
    objectIdType1 : {
        type : Schema.Types.ObjectId,
        ref: 'usuario'
    },
    objectIdType2 : {
        type : Schema.Types.ObjectId,
        ref: 'usuario'
    },
    numberType1 : {
        type : Number,
        required : true,
        default: 1000
    },
    numberType2 : {
        type : Number,
        required: true,
        default: 2000
    },
    boolType1 : {
        type: Boolean,
        required : true,
        default : true
    },
    boolType2 : {
        type: Boolean,
        required: true,
        default : false
    },
    arrType1 : {
        type: Array,
        required: true,
        default : [new Date()]
    },
    arrType2 : {
        type: Array,
        required: true,
        default: [{
            key1 : {
                type: String,
                required: true,
                default: 'key1'
            },
            key2 : {
                type: String,
                required: true,
                default: 'key2'
            }
        }]
    },
    arrType3 : {
        type: Array,
        required: true,
        default: [{
            type: Array,
            required: true,
            default : [new Date()]
        }]
    },
    arrType4 : {
        type: Array,
        required: true,
        default : [{
            type: Array,
            required: true,
            default : [{
                key1 : {
                    type: String,
                    required: true,
                    default: 'key1'
                }
            }]
        }]
    },
    arrType5 : {
        type: Array,
        required: true,
        default : [{
            type: Array,
            required: true,
            default : [{
                type: Array,
                required: true,
                default : [{
                    key1 : {
                        type: String,
                        required: true,
                        default: 'key1'
                    },
                    key2 : {
                        type: String,
                        required: true,
                        default: 'key2'
                    }
                }]
            }]
        }]
    },
    arrType6: {
        type: Array,
        required: true,
        default : [{
            type: Array,
            required: true,
            default : [{
                type: Array,
                required: true,
                default : [{
                    type: Array,
                    required: true,
                    default : [new Date()]
                }]
            }]
        }]
    }
},{
    versionKey: false,
    collection: 'practice'
});

export const PracticeModel = model<IPractice>('PracticeModel', practiceSchema);









// array of dates, sort by date
    // array of objects with date, sort by date 
    // property1 and property2 get for either one $and $or
    // match two simple params
    // get document $sample
    // controller to get paginated array, sorted by object date attrib 
    // match for passed time (lit)