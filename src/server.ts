import { HapiServer } from './classes/hapiserver.class';
import { database } from './database';
import { Yaml } from './helpers/yaml.helper';

import { user_routes } from './routes/user.routes';
import { auth_routes } from './routes/auth.routes';
import { auth_strategies } from './auth/auth-strategies';
import { cronjobs } from './scripts/cronjobs';
import { practice_routes } from './routes/practice.routes';

const init = async() => {

    const Config = Yaml();

    const server = new HapiServer({
        port: Config.Server.port,
        host: Config.Server.host,
        routes: {
            cors:{
                origin: [Config.Server.cors],
                additionalHeaders: [Config.Server.JWT_Header]
            }
        }
    });

    server.config = Config;

    await auth_strategies(server);

    server.route([...user_routes, ...auth_routes, ...practice_routes]);
    await database(server);

    await server.start();
    console.log(`Server executing on ${server.info.uri}`);
    await cronjobs();
    console.log(`Cron jobs init`);
}

process.on("unhandledRejection", error => {
    console.log(`Server blew up to infinity and beyond\n${error}`);
    process.exit(0);
})

init();