import { deleteUser, getUser, postUser, putUser } from "../controllers/user.controllers";
import { Options_DELETE_User, Options_GET_User, Options_POST_User, Options_PUT_User } from "../helpers/user.helpers";


/**
* User routes imported into server.route()
* @exports user_routes 
**/
export const user_routes = [
    {
        method: 'GET',
        path: '/user/{_id}',
        handler: getUser,
        options: Options_GET_User
    },
    {
        method: 'PUT',
        path: '/user/{_id}',
        handler: putUser,
        options: Options_PUT_User
    },
    {
        method: 'DELETE',
        path: '/user/{_id}',
        handler: deleteUser,
        options: Options_DELETE_User
    },
    {
        method: 'POST',
        path: '/user',
        handler: postUser,
        options: Options_POST_User
    }
];
