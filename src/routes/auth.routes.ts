import { login } from "../controllers/auth.controller";
import { Options_POST_Login } from "../helpers/post.auth.helper";


/**
* Authentication routes for user imported into server.route()
* @exports auth_routes 
**/
export const auth_routes = [
    {
        method: 'POST',
        path: '/login',
        handler: login,
        options: Options_POST_Login
    }
]