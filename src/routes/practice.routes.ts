import { postPractice } from "../controllers/practice.controller";
import { postPractice_Options } from "../helpers/practice.helper";

export const practice_routes = [
    {
        method: 'POST',
        path: '/practice',
        handler: postPractice,
        options: postPractice_Options
    }
]