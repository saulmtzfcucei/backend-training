"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const mongoose_1 = require("mongoose");
/**
* User schema for 'usuario' collection, of type
* `<IUser>`. It does not allow versionKeys.
* Includes the types:
* * username : `{type:String, required: true}`
* * email : `{type:String, required: true}`
* * password? : `{type:String}`
* * role `{ type:String, required:true}`
* * deleted `{type: Boolean, default:false, required:true}`
* @see `IUser`
**/
const userSchema = new mongoose_1.Schema({
    username: { type: String, unique: true, required: true },
    email: { type: String, unique: true, lowercase: true, required: true },
    password: { type: String },
    role: { type: String, required: true },
    deleted: { type: Boolean, default: false, required: true }
}, {
    versionKey: false,
    collection: 'usuario'
});
exports.User = (0, mongoose_1.model)('User', userSchema);
