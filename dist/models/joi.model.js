"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.failAction = exports.JoiSchema = void 0;
const boom_1 = __importDefault(require("@hapi/boom"));
const joi_class_1 = __importDefault(require("./../classes/joi.class"));
/**
* Definition of all Joi option objects, including:
* * Validation of user params
* * Validation of token
* * Validation of methods: `get`, `post`, `login`, `put`
* @exports JoiSchema
**/
exports.JoiSchema = {
    user: {
        params: {
            _id: joi_class_1.default.ObjectId().required(),
            username: joi_class_1.default.Username().required(),
            email: joi_class_1.default.Email().required(),
            password: joi_class_1.default.UserPassword().required(),
            role: joi_class_1.default.Role().required(),
            deleted: joi_class_1.default.boolean().required(),
            token: joi_class_1.default.object({ token: joi_class_1.default.JWT().required() })
        },
        methods: {
            get: joi_class_1.default.object({
                _id: joi_class_1.default.ObjectId().required(),
                username: joi_class_1.default.Username().required(),
                email: joi_class_1.default.Email().required(),
                role: joi_class_1.default.Role().required(),
                deleted: joi_class_1.default.boolean().required(),
            }),
            post: joi_class_1.default.object({
                username: joi_class_1.default.Username().required(),
                email: joi_class_1.default.Email().required(),
                password: joi_class_1.default.UserPassword().required(),
                role: joi_class_1.default.Role().required(),
            }),
            login: joi_class_1.default.object({
                email: joi_class_1.default.Email().required(),
                password: joi_class_1.default.UserPassword().required()
            }),
            put: joi_class_1.default.alternatives().try(joi_class_1.default.object({ username: joi_class_1.default.Username().required() }), joi_class_1.default.object({ email: joi_class_1.default.Email().required() }), joi_class_1.default.object({ password: joi_class_1.default.UserPassword().required() }), joi_class_1.default.object({ role: joi_class_1.default.Role().required() })).required()
        }
    }
};
/**
* failAction function if a validation using `JoiSchemas` failed
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit.
* @param error - To receive errors.
* @see `JoiSchema`
* @exports `failAction`
* @return
* * A 401 Unauthorized  error response.
* * A 500 Bad Implementation error response.
**/
exports.failAction = ((req, h, error) => {
    return error.isJoi ?
        boom_1.default.unauthorized(error.details[0].message) :
        boom_1.default.badImplementation();
});
