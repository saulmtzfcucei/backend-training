"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const hapiserver_class_1 = require("./classes/hapiserver.class");
const database_1 = require("./database");
const yaml_helper_1 = require("./helpers/yaml.helper");
const user_routes_1 = require("./routes/user.routes");
const auth_routes_1 = require("./routes/auth.routes");
const auth_strategies_1 = require("./auth/auth-strategies");
const init = () => __awaiter(void 0, void 0, void 0, function* () {
    const Config = (0, yaml_helper_1.Yaml)();
    const server = new hapiserver_class_1.HapiServer({
        port: Config.Server.port,
        host: Config.Server.host,
        routes: {
            cors: {
                origin: [Config.Server.cors],
                additionalHeaders: [Config.Server.JWT_Header]
            }
        }
    });
    server.config = Config;
    yield (0, auth_strategies_1.auth_strategies)(server);
    server.route([...user_routes_1.user_routes, ...auth_routes_1.auth_routes]);
    yield (0, database_1.database)(server);
    yield server.start();
    console.log(`Server executing on ${server.info.uri}`);
});
process.on("unhandledRejection", error => {
    console.log(`Server blew up to infinity and beyond\n${error}`);
    process.exit(0);
});
init();
