"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.user_routes = void 0;
const user_controllers_1 = require("../controllers/user.controllers");
const user_helpers_1 = require("../helpers/user.helpers");
/**
* User routes imported into server.route()
* @exports user_routes
**/
exports.user_routes = [
    {
        method: 'GET',
        path: '/user/{_id}',
        handler: user_controllers_1.getUser,
        options: user_helpers_1.Options_GET_User
    },
    {
        method: 'PUT',
        path: '/user/{_id}',
        handler: user_controllers_1.putUser,
        options: user_helpers_1.Options_PUT_User
    },
    {
        method: 'DELETE',
        path: '/user/{_id}',
        handler: user_controllers_1.deleteUser,
        options: user_helpers_1.Options_DELETE_User
    },
    {
        method: 'POST',
        path: '/user',
        handler: user_controllers_1.postUser,
        options: user_helpers_1.Options_POST_User
    }
];
