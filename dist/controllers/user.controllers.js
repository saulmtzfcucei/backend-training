"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.putUser = exports.postUser = exports.getUser = void 0;
const boom_1 = __importDefault(require("@hapi/boom"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const user_model_1 = require("../models/user.model");
/**
* Controller for getting one user from the database with a provided `id`.
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit.
* @return
* * A 200 OK response with the obtained resource `{user}`
* * A 404 Not Found error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
const getUser = (req, h) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield user_model_1.User.findById(req.params._id).where({ deleted: false }).select('-password');
        return user ?
            h.response(user === null || user === void 0 ? void 0 : user.toJSON()).code(200) :
            boom_1.default.notFound('No hay usuarios con ese id');
    }
    catch (error) {
        return boom_1.default.badImplementation();
    }
});
exports.getUser = getUser;
/**
* Controller for posting a user in the database.
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit.
* @return
* * A 200 OK response with the inserted resource `{user}`
* * A 400 Bad Request error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
const postUser = (req, h) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const salt = bcrypt_1.default.genSaltSync();
        req.payload.password = bcrypt_1.default.hashSync(req.payload.password, salt);
        const user = yield new user_model_1.User(req.payload).save();
        user.password = undefined;
        return user ?
            h.response(user === null || user === void 0 ? void 0 : user.toJSON()).code(200) :
            boom_1.default.badImplementation();
    }
    catch (error) {
        return error.code === 11000 ?
            boom_1.default.badRequest(`El ${Object.keys(JSON.parse(JSON.stringify(error.keyValue)))[0]} ya ha sido utilizado`) :
            boom_1.default.badImplementation();
    }
});
exports.postUser = postUser;
/**
* Controller for updating the username of a resource in the database.
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The hapi response toolkit.
* @return
* * A 200 OK response with the updated resource `{user}`
* * A 400 Bad Request error response.
* * A 404 Not Found error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
const putUser = (req, h) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (req.payload.password) {
            const salt = bcrypt_1.default.genSaltSync();
            req.payload.password = bcrypt_1.default.hashSync(req.payload.password, salt);
        }
        const updatedUser = yield user_model_1.User.findByIdAndUpdate(req.params._id, req.payload, { new: true }).where({ deleted: false }).select('-password');
        return updatedUser ?
            h.response(updatedUser === null || updatedUser === void 0 ? void 0 : updatedUser.toJSON()).code(200) :
            boom_1.default.notFound('No hay usuarios con ese ID');
    }
    catch (error) {
        return error.code === 11000 ?
            boom_1.default.badRequest(`El ${Object.keys(JSON.parse(JSON.stringify(error.keyValue)))[0]} ya ha sido utilizado`) :
            boom_1.default.badImplementation();
    }
});
exports.putUser = putUser;
/**
* Controller for deleting one user from the database.
* @param req - Is the hapi request object of the request which is being authenticated
* @param h - The response toolkit.
* @return
* * A 200 OK response with the updated resource `{user}`
* * A 404 Not Found error response.
* * A 500 Bad Implementation error response.
* @see `JoiSchema`
**/
const deleteUser = (req, h) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const deletedUser = yield user_model_1.User.findByIdAndUpdate(req.params._id, { $set: { deleted: true } }, { new: true }).where({ deleted: false }).select('-password');
        return deletedUser ?
            h.response(deletedUser === null || deletedUser === void 0 ? void 0 : deletedUser.toJSON()).code(200) :
            boom_1.default.notFound(`No existe un usuario con dicho ID`);
    }
    catch (error) {
        return boom_1.default.badImplementation();
    }
});
exports.deleteUser = deleteUser;
