"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const ObjectIdPattern = /^[0-9a-fA-F]{24}$/;
const UserUsernamePattern = /^[0-9a-zA-Z]{4,15}$/;
const EmailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const UserPasswordPattern = /^\w(?=.*?[#?!@$%^&*-]).{6,20}$/;
const JWTPattern = /(^[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*\.[A-Za-z0-9-_]*$)/;
const RolePattern = /(^[A-Z]{2,15}$)/;
/**
* Extends Joi class to accept `Joi.ObjectId()`, `Joi.UserUsername()`,
* `Joi.UserEmail()`, `Joi.UserPassword()` and `Joi.JWT()` through
* testing regexs for each.
* @extends Joi
* @param  joi.Extension[]
* @return Result of Joi validation `{value , errors}`
* @see `JoiSchema`
**/
const Joi = joi_1.default.extend({
    type: 'ObjectId',
    messages: { invalid: 'Field must be a valid ObjectId.' },
    validate(value, { error }) {
        if (!ObjectIdPattern.test(String(value)))
            return { value, errors: error('invalid') };
    }
}, {
    type: 'Username',
    messages: { invalid: 'Field must be a valid username.' },
    validate(value, { error }) {
        if (!UserUsernamePattern.test(String(value)))
            return { value, errors: error('invalid') };
    }
}, {
    type: 'Email',
    messages: { invalid: 'Field must be a valid email.' },
    validate(value, { error }) {
        if (!EmailPattern.test(String(value)))
            return { value, errors: error('invalid') };
    }
}, {
    type: 'UserPassword',
    messages: { invalid: 'Field must be a valid password.' },
    validate(value, { error }) {
        if (!UserPasswordPattern.test(String(value)))
            return { value, errors: error('invalid') };
    }
}, {
    type: 'JWT',
    messages: { invalid: 'Field must be a valid JWT.' },
    validate(value, { error }) {
        if (!JWTPattern.test(String(value)))
            return { value, errors: error('invalid') };
    }
}, {
    type: 'Role',
    messages: { invalid: 'Field must be a valid role' },
    validate(value, { error }) {
        if (!RolePattern.test(String(value)))
            return { value, errors: error('invalid') };
    }
});
exports.default = Joi;
