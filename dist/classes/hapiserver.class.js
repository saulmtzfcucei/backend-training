"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HapiServer = void 0;
const hapi_1 = __importDefault(require("@hapi/hapi"));
/**
* Extends `Hapi.Server` class to accept costumizable `config` attribute.
* @extends Hapi.Server
**/
class HapiServer extends hapi_1.default.Server {
}
exports.HapiServer = HapiServer;
