"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Yaml = void 0;
const yaml_1 = __importDefault(require("yaml"));
const fs_1 = __importDefault(require("fs"));
/**
* Function that returns server variables defined in a YAML file.
* @return `JSON` object containing the server variables.
**/
function Yaml() {
    const file = fs_1.default.readFileSync('./server-variables.yml', 'utf-8');
    return yaml_1.default.parse(file);
}
exports.Yaml = Yaml;
