"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generarJWT = void 0;
const boom_1 = __importDefault(require("@hapi/boom"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const yaml_helper_1 = require("../helpers/yaml.helper");
/**
* Function that generates a JWT.
* @param  _id Receives a user id to generate a JWT.
* @return A generated `token`
**/
const generarJWT = (_id) => {
    try {
        const { Auth } = (0, yaml_helper_1.Yaml)();
        return jsonwebtoken_1.default.sign({ _id }, Auth.auth_secret, {
            expiresIn: Auth.expiration_time
        });
    }
    catch (error) {
        return boom_1.default.badImplementation();
    }
};
exports.generarJWT = generarJWT;
